
import SpriteKit

class MainMenu: SKScene {
    var lvl1: SKSpriteNode!
    var lvl2: SKSpriteNode!
    var lvl3: SKSpriteNode!
    var lvl11: SKSpriteNode!
    var lvl22: SKSpriteNode!
    var lvl33: SKSpriteNode!
    var play: SKSpriteNode!
    var level: String!
    override func didMove(to view: SKView) {
//        let sound = SKAction.playSoundFileNamed("bg.mp3", waitForCompletion: false)
//
////        let loopSound = SKAction.repeatForever(sound)
//
//        self.run(sound)
//        let backgroundSound = SKAudioNode(fileNamed: "bg.mp3")
//        self.addChild(backgroundSound)
//            backgroundSound.run(SKAction.play())
//        MusicPlayer.shared.startBackgroundMusic()
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let location = touch.location(in: self)
            let nodesArray = self.nodes(at: location)
            
            lvl11 = self.childNode(withName: "lvl11") as? SKSpriteNode
            lvl22 = self.childNode(withName: "lvl22") as? SKSpriteNode
            lvl33 = self.childNode(withName: "lvl33") as? SKSpriteNode
            
            if nodesArray.first?.name == "play"{
                startGame()
            }
            if nodesArray.first?.name == "choose_bee"{
                chooseBee()
            }
            if nodesArray.first?.name == "lvl1"{
                GameScene.level = "1"
                lvl11.zPosition = -1
                lvl22.zPosition = -3
                lvl33.zPosition = -3

            }
            if nodesArray.first?.name == "lvl2"{
                GameScene.level = "2"
                lvl11.zPosition = -3
                lvl22.zPosition = -1
                lvl33.zPosition = -3

            }
            if nodesArray.first?.name == "lvl3"{
                GameScene.level = "3"
                lvl11.zPosition = -3
                lvl22.zPosition = -3
                lvl33.zPosition = -1

            }
        }
    }
    func startGame(){
        let transition = SKTransition.fade(withDuration: 0.5)
        if let scene = GameScene(fileNamed: "GameScene"){
            scene.scaleMode = .aspectFit
            self.view?.presentScene(scene, transition: transition)
        }
        
    }
    func chooseBee(){
        let transition = SKTransition.fade(withDuration: 0.5)
        if let scene = GameScene(fileNamed: "NewBee"){
            scene.scaleMode = .aspectFit
            self.view?.presentScene(scene, transition: transition)
        }
        
    }
}
