import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
//    var bee: SKSpriteNode!
    
    var gameTimer: Timer!
    var field_2_button: SKSpriteNode!

    let velocityMultiplier: CGFloat = 0.12
    
    let circleCategory:UInt32 = 0x1 << 0
    let beeCategory:UInt32 = 0x1 << 1
    
    var flowers = ["fl1", "fl2", "fl3"]
    static var level = "2"
    
    lazy var scorelabel: SKLabelNode = {
        let sclbl = SKLabelNode(text: "Счет: 0")
        sclbl.fontName = "AmericanTypewriter-Bold"
        sclbl.fontSize = 50
        sclbl.fontColor = UIColor.white
        sclbl.position = CGPoint(x: 100, y: UIScreen.main.bounds.height - 100)
        return sclbl
    }()
    var score:Int = 0{
        didSet{
            scorelabel.text = "Счет: \(score)"
        }
    }
    
    let defaults = UserDefaults.standard

    lazy var bee: SKSpriteNode = {
        
        let f0 = SKTexture.init(imageNamed: "bee_0")
        let f1 = SKTexture.init(imageNamed: "bee_1")
        let frames: [SKTexture] = [f0, f1]
        
        let b: SKSpriteNode!
        
        // Load the first frame as initialization
        if defaults.integer(forKey: "bee") == 2 {
            b = SKSpriteNode(imageNamed: "bee_2")
            b.position = CGPoint(x: 0, y: 0)
            b.setScale(0.17)
        }else{
            b = SKSpriteNode(imageNamed: "bee_0")
            b.position = CGPoint(x: 0, y: 0)
            b.setScale(0.5)
            // Change the frame per 0.2 sec
            let animation = SKAction.animate(with: frames, timePerFrame: 0.3)
            b.run(SKAction.repeatForever(animation))
        }

        b.physicsBody = SKPhysicsBody(rectangleOf: b.size)
        
        b.physicsBody?.isDynamic = true
        
        
        b.physicsBody?.categoryBitMask = beeCategory
        b.physicsBody?.contactTestBitMask = circleCategory
        b.physicsBody?.collisionBitMask = 0
        b.physicsBody?.usesPreciseCollisionDetection = true
        return b
    }()
    
      lazy var analogJoystick: AnalogJoystick = {
        let js = AnalogJoystick(diameter: 100, colors: (UIColor(red: 1, green: 165/255, blue: 0, alpha: 1), UIColor(red: 1, green: 100/255, blue: 20, alpha: 1)))
        js.position = CGPoint(x: self.size.width * -0.45 + js.radius + 45, y: self.size.height * -0.45 + js.radius + 45)
        js.zPosition = 3
        return js
      }()
    
    override func didMove(to view: SKView) {
        
        self.addChild(bee)
        
        self.physicsWorld.gravity = CGVector(dx:0, dy:0)
        self.physicsWorld.contactDelegate = self
        
        var speed = 0.5
        if GameScene.level == "1"{
            speed = 0.8
        }
        if GameScene.level == "2"{
            speed = 0.4
        }
        if GameScene.level == "3"{
            speed = 0.1
        }
        
        gameTimer = Timer.scheduledTimer(timeInterval: speed, target: self, selector: #selector(addCircles), userInfo: nil, repeats: true)
        
        score = defaults.integer(forKey: "score")
        self.addChild(scorelabel)
        setupJoystick()
    }
    override func update(_ currentTime: TimeInterval) {
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let location = touch.location(in: self)
//            moveBee(location: location)
            
            let nodesArray = self.nodes(at: location)
            
            if nodesArray.first?.name == "Field2Btn"{
                changeFon()
            }
        }
        
        
    }
    func changeFon(){
        let transition = SKTransition.fade(withDuration: 0.5)
        if let scene = SecondField(fileNamed: "SecondField"){
            scene.scaleMode = .aspectFit
            self.view?.presentScene(scene, transition: transition)
        }
        
    }
//    func moveBee(location: CGPoint){
//        let animSpeed:TimeInterval = 0.3
//        let x = location.x
//        let y = location.y
//
//        let location_2 = CGPoint(x:x, y:y)
//        let action = SKAction.move(to: location_2, duration: animSpeed)
//        print(x, y, location)
//        bee.run(action)
//    }
    @objc func addCircles(){
        flowers = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: flowers) as! [String]
        
        let circle = SKSpriteNode(imageNamed: flowers[0])
        
        let randomPosX = GKRandomDistribution(lowestValue: -250, highestValue: Int(UIScreen.main.bounds.size.width - 180))
        let posX = CGFloat(randomPosX.nextInt())
        let randomPosY = GKRandomDistribution(lowestValue: -550, highestValue: Int(UIScreen.main.bounds.size.height - 400))
        let posY = CGFloat(randomPosY.nextInt())
        circle.position = CGPoint(x: posX, y: posY)
        circle.setScale(0.5)
        circle.physicsBody = SKPhysicsBody(circleOfRadius: circle.size.width / 4)
        circle.physicsBody?.isDynamic = true
        circle.physicsBody?.categoryBitMask = circleCategory
        circle.physicsBody?.contactTestBitMask = beeCategory
        circle.physicsBody?.collisionBitMask = 0
        circle.physicsBody?.usesPreciseCollisionDetection = true
        
        circle.zPosition = -1
        self.addChild(circle)
        
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        var bigBody:SKPhysicsBody
        var smallBody:SKPhysicsBody
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            smallBody = contact.bodyA
            bigBody = contact.bodyB
        } else {
            smallBody = contact.bodyB
            bigBody = contact.bodyA
        }
        
        if (bigBody.categoryBitMask & beeCategory) != 0
            && (smallBody.categoryBitMask & circleCategory) != 0{
            let action1 = SKAction.wait(forDuration: 0.5)
            let action2 = SKAction.run {
                smallBody.node?.removeFromParent()
                self.score += 1
                
                self.defaults.set(self.score, forKey: "score")
            }
            let otherSequence = SKAction.sequence([action1, action2])
            self.run(otherSequence)
        }
    }
    
    func setupJoystick() {
          addChild(analogJoystick)
      
          analogJoystick.trackingHandler = { [unowned self] data in
            self.bee.position = CGPoint(x: self.bee.position.x + (data.velocity.x * self.velocityMultiplier),
                                         y: self.bee.position.y + (data.velocity.y * self.velocityMultiplier))
//            self.bee.zRotation = data.angular
          }
      
    }
}


