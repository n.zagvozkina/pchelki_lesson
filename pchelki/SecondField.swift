import SpriteKit
import GameplayKit


class SecondField: SKScene, SKPhysicsContactDelegate {
    var bee: SKSpriteNode!
    
    
    let circleCategory:UInt32 = 0x1 << 0
    let beeCategory:UInt32 = 0x1 << 1
    var gameTimer: Timer!
    var flowers = ["fl_21", "fl_22", "fl_23", "fl_24"]
    var field_2_button: SKSpriteNode!
    var scorelabel:SKLabelNode!
    var score:Int = 0{
        didSet{
            scorelabel.text = "Счет: \(score)"
        }
    }
    
    let defaults = UserDefaults.standard
    
    override func didMove(to view: SKView) {
//        MusicPlayer.shared.startBackgroundMusic()
        let f0 = SKTexture.init(imageNamed: "bee_0")
        let f1 = SKTexture.init(imageNamed: "bee_1")
        let frames: [SKTexture] = [f0, f1]
        if defaults.integer(forKey: "bee") == 2 {
            bee = SKSpriteNode(imageNamed: "bee_2")
            bee.position = CGPoint(x: 0, y: 0)
            bee.setScale(0.17)
        }else{
            bee = SKSpriteNode(imageNamed: "bee_0")
            bee.position = CGPoint(x: 0, y: 0)
            bee.setScale(0.5)
            // Change the frame per 0.2 sec
            let animation = SKAction.animate(with: frames, timePerFrame: 0.3)
            bee.run(SKAction.repeatForever(animation))
        }
        bee.physicsBody = SKPhysicsBody(rectangleOf: bee.size)
        
        bee.physicsBody?.isDynamic = true
        
        
        bee.physicsBody?.categoryBitMask = beeCategory
        bee.physicsBody?.contactTestBitMask = circleCategory
        bee.physicsBody?.collisionBitMask = 0
        bee.physicsBody?.usesPreciseCollisionDetection = true
        
        self.addChild(bee)
        field_2_button = self.childNode(withName: "Field1Btn") as? SKSpriteNode
        
        self.physicsWorld.gravity = CGVector(dx:0, dy:0)
        self.physicsWorld.contactDelegate = self
        
        var speed = 0.5
        if GameScene.level == "1"{
            speed = 0.8
        }
        if GameScene.level == "2"{
            speed = 0.4
        }
        if GameScene.level == "3"{
            speed = 0.1
        }
        gameTimer = Timer.scheduledTimer(timeInterval: speed, target: self, selector: #selector(addCircles), userInfo: nil, repeats: true)
        
        scorelabel = SKLabelNode(text: "Счет: 0")
        scorelabel.fontName = "AmericanTypewriter-Bold"
        scorelabel.fontSize = 50
        scorelabel.fontColor = UIColor.white
        scorelabel.position = CGPoint(x: 100, y: UIScreen.main.bounds.height - 100)
        score = defaults.integer(forKey: "score")
        self.addChild(scorelabel)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let location = touch.location(in: self)
            moveBee(location: location)
            let nodesArray = self.nodes(at: location)
            
            if nodesArray.first?.name == "Field1Btn"{
                changeFon()
            }
        }
        
        
    }
    func changeFon(){
        let transition = SKTransition.fade(withDuration: 0.5)
        if let scene = SecondField(fileNamed: "GameScene"){
            
            scene.scaleMode = .aspectFit
            self.view?.presentScene(scene, transition: transition)
        }
    }
    func moveBee(location: CGPoint){
        let animSpeed:TimeInterval = 0.3
        let x = location.x
        let y = location.y
        
        let location_2 = CGPoint(x:x, y:y)
        let action = SKAction.move(to: location_2, duration: animSpeed)
        print(x, y, location)
        bee.run(action)
    }
    @objc func addCircles(){
        flowers = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: flowers) as! [String]
        
        let circle = SKSpriteNode(imageNamed: flowers[0])
        
        let randomPosX = GKRandomDistribution(lowestValue: -250, highestValue: Int(UIScreen.main.bounds.size.width - 180))
        let posX = CGFloat(randomPosX.nextInt())
        let randomPosY = GKRandomDistribution(lowestValue: -550, highestValue: Int(UIScreen.main.bounds.size.height - 400))
        let posY = CGFloat(randomPosY.nextInt())
        circle.position = CGPoint(x: posX, y: posY)
        circle.setScale(0.5)
        circle.physicsBody = SKPhysicsBody(circleOfRadius: circle.size.width / 4)
        circle.physicsBody?.isDynamic = true
        circle.physicsBody?.categoryBitMask = circleCategory
        circle.physicsBody?.contactTestBitMask = beeCategory
        circle.physicsBody?.collisionBitMask = 0
        circle.physicsBody?.usesPreciseCollisionDetection = true
        
        circle.zPosition = -1
        self.addChild(circle)
        
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        var bigBody:SKPhysicsBody
        var smallBody:SKPhysicsBody
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            smallBody = contact.bodyA
            bigBody = contact.bodyB
        } else {
            smallBody = contact.bodyB
            bigBody = contact.bodyA
        }
        
        if (bigBody.categoryBitMask & beeCategory) != 0
            && (smallBody.categoryBitMask & circleCategory) != 0{
            let action1 = SKAction.wait(forDuration: 0.5)
            let action2 = SKAction.run {
                smallBody.node?.removeFromParent()
                self.score += 1
                self.defaults.set(self.score, forKey: "score")
            }
            let otherSequence = SKAction.sequence([action1, action2])
            self.run(otherSequence)
        }
    }
    
    
    
}
