//
//  MusicPlayer.swift
//  pchelki
//
//  Created by Nadezhda Zagvozkina on 14.07.2020.
//  Copyright © 2020 Nadezhda Zagvozkina. All rights reserved.
//

import Foundation
import AVFoundation

class MusicPlayer {
    static let shared = MusicPlayer()
    var audioPlayer: AVAudioPlayer?

    func startBackgroundMusic() {
        if let bundle = Bundle.main.path(forResource: "bg", ofType: "mp3") {
                   let backgroundMusic = NSURL(fileURLWithPath: bundle)
                   do {
                       audioPlayer = try AVAudioPlayer(contentsOf:backgroundMusic as URL)
                       guard let audioPlayer = audioPlayer else { return }
                       audioPlayer.numberOfLoops = -1
                       audioPlayer.prepareToPlay()
                       audioPlayer.play()
                   } catch {
                       print(error)
                   }
               }
    }

}
