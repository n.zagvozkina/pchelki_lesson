//
//  NewBee.swift
//  pchelki
//
//  Created by Nadezhda Zagvozkina on 08.07.2020.
//  Copyright © 2020 Nadezhda Zagvozkina. All rights reserved.
//

import SpriteKit

class NewBee: SKScene {
    let defaults = UserDefaults.standard
    override func didMove(to view: SKView) {
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let location = touch.location(in: self)
            let nodesArray = self.nodes(at: location)
            
            if nodesArray.first?.name == "bee_1"{
                defaults.set(1, forKey: "bee")
            }
            if nodesArray.first?.name == "bee_2"{
                defaults.set(2, forKey: "bee")
            }
            if nodesArray.first?.name == "back"{
                back()
            }
        }
        
        
    }
    func back(){
        let transition = SKTransition.fade(withDuration: 0.5)
        if let scene = GameScene(fileNamed: "MainMenu"){
            scene.scaleMode = .aspectFit
            self.view?.presentScene(scene, transition: transition)
        }
        
    }

}
